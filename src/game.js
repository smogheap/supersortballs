//credits

var GAME = {
	savefile: {
		seed: "",
		showhelp: true,
		showstreak: true,
		showtimer: false,
		openbins: false,
		colorblind: false,
		soundfx: false,
		autopile: false,
		maxundo: 1,

		hold: null,
		state: null,
		level: null,
		timer: null,
		bins: 8,
		depth: 4,

		streak: 0,
		streakmax: 0,
		streakwon: {}, //seed: time
		totaltime: 0,
		totalgames: 0,
		seedgames: 0
	},
	palette: {
		"rainbows": {
			"small": [
				"#f00", "#ff0", "#0f0", "#0ff", "#06f"
			],
			"full": [
				"#f8b", "#f00", "#f80", "#ff0", "#0f0", "#0ff", "#06f", "#b0f"
			]
		},
		"pride flags": {
			"gay pride_h": [
				"#000", "#751", "#e00", "#f80", "#fe0", "#082", "#05f", "#708"
			],
			"gay pride_h": [
				"#000", "#751", "#e00", "#f80", "#fe0", "#082", "#05f", "#708"
			],
			"trans pride_h": [
				"#5cf", "#fab", "#fff", "#fab", "#5cf"
			],
			"bi pride_h": [
				"#d07", "#759", "#03a"
			],
			"inter pride_h": [
				"#fd0", "#70a"
			],
			"pan pride_h": [
				"#f28", "#fd0", "#2bf"
			],
			"nonbinary pride_h": [
				"#ff3", "#fff", "#95c", "#333"
			],
			"genderfluid pride_h": [
				"#f79", "#fff", "#b1d", "#000", "#33b"
			],
			"genderqueer pride_h": [
				"#b7d", "fff", "#4b82"
			],
			"agender pride_h": [
				"#000", "#bbb", "#fff", "#bf8", "#fff", "#bbb", "#000"
			]
		},
		"national flags": {
			"armenia_h": [
				"#da000a", "#0030a1", "#f2aa00"
			],
			"belgium": [
				"#fff", "#ee2436", "#fae140"
			],
			"bolivia_h": [
				"#d62718", "#f9e400", "#007a31"
			],
			"bulgaria_h": [
				"#fff", "#00976e", "#d7210a"
			],
			"chad": [
				"#002164", "#fecc00", "#c7042c"
			],
			"estonia_h": [
				"#0073cf", "#000", "#fff"
			],
			"france": [
				"#002395", "white", "#ed2939"
			],
			"gabon_h": [
				"#009f60", "#fcd20e", "#3776c5"
			],
			"germany_h": [
				"#000", "#fff", "#de0000"
			],
			"guinea": [
				"#cf0821", "#fcd20e", "#009560"
			],
			"hungary_h": [
				"#ce253c", "#fff", "#ff9933"
			],
			"ireland": [
				"#0e9c62", "#fff", "#ff893c"
			],
			"italy": [
				"#009246", "#fff", "#ce2b37"
			],
			"ivory coast": [
				"#f78000", "#fff", "#009f60"
			],
			"luxembourg_h": [
				"#ee2436", "#fff", "#00a3df"
			],
			"mali": [
				"#0cb637", "#fcd20e", "#cf0821"
			],
			"mauritius_h": [
				"#eb2336", "#131a6d", "#ffd600", "#00a750"
			],
			"netherlands_h": [
				"#ae1c28", "#fff", "#21468b"
			],
			"nigeria": [
				"#008751", "#fff", "#008751"
			],
			"peru": [
				"#da081e", "#fff", "#da081e"
			],
			"romania": [
				"#002b7f", "#fcd116", "#ce1126"
			],
			"russian federation_h": [
				"#fff", "#0039a6", "#d52b1e"
			],
			"sierra leone_h": [
				"#17b637", "#fff", "#0073c7"
			],
			"thailand_h": [
				"#292549", "#f4f5f8", "#292549", "#292549", "#f4f5f8", "#292549"
			],
			"ukraine_h": [
				"#005bbb", "#ffd500"
			],
			"yemen_h": [
				"#cf0821", "#fff", "#000"
			]
		},
		"palettes": {
			"cga1": [
				"#000", "#59fffc", "#ef2af8", "$fff"
			],
			"cga2": [
				"#000", "#58ff4f", "#ee364b", "#fdff52"
			],
			"game boy": [
				"#0f380e", "#30622f", "#9bbb0e", "#cadca0"
			],
			"virtual boy": [
				"#000", "#500", "#a00", "#f00"
			],
			"zx spectrum": [
				"#000", "#00f", "#f00", "#f0f", "#0f0", "#0ff", "#ff0", "#fff"
			],
			"colorblind-friendly": [
				"#044", "#06d", "#409", "#900", "#d60", "#2f2", "#ff6", "#bdf"
			]
		}
	}
};


var DEPTH = 4;
var BINS = 8;
var LEVEL = [];
var SEED = "1";
var FROMSEED = false;
var SHUFFLING = false;
var TIMER = 0;
var STARTED = 0;

var lastrand = 1;
var prev = [];

function rand() {
	lastrand = lastrand * 16807 % 2147483647;
	return lastrand;
}
function hash(b) {
	// turn any string into a seed
    for (var a = 0, c = b.length; c--;) {
		a += b.charCodeAt(c);
		a += a << 10;
		a ^= a >> 6;
    }
    a += a << 3;
    a ^= a >> 11;
    return((a+(a<<15)&4294967295)>>>0);
}
function genstring() {
	var word = "";
	var chars = [
		"a", "b", "c", "d", "e", "f", "g", "h", "j", "k", "m", "n",
		"p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z",
		"1", "2", "3", "4", "5", "6", "7", "8", "9", "0"
	];
	for(var i = 0; i < 4; i++) {
		word += chars[Math.floor(Math.random() * chars.length)];
	}
	word += chars[document.querySelector("#bins select").selectedIndex];
	word += chars[document.querySelector("#depth select").selectedIndex];
	return word;

	/*
	var word = "";
	var cons = [
		"b", "bl", "br", "c", "cl", "cr", "d", "dr", "f", "fr", "g", "gl", "gr",
		"h", "j", "k", "kr", "l", "m", "n", "p", "pl", "pr", "qu", "r",
		"s", "sl", "t", "tr", "v", "vr", "w", "y", "z"
	];
	var vows = [ "a", "e", "ee", "i", "o", "oi", "ou", "u" ];
	var i = 1 + Math.floor(Math.random() * 4);
	for (i; i > 0; i--) {
		word += cons[Math.floor(Math.random() * cons.length)];
		word += vows[Math.floor(Math.random() * vows.length)];
	}
	return word;
	*/
}

function load() {
	unwin();
	document.querySelectorAll(".column").forEach(function(col, idx) {
		col.childNodes.forEach(function(node, pos) {
			node.className = LEVEL[idx][pos];
		});
	});
	document.querySelector("#hold div").className = "empty";
	prev = [];
	document.querySelector("#undo").disabled = true;
	document.querySelector("#shuffle").classList.add("hidden");
}
function save() {
	LEVEL = [];
	document.querySelectorAll(".column").forEach(function(col, idx) {
		var list = [];
		col.childNodes.forEach(function(node) {
			list.push(node.className);
		});
		LEVEL.push(list);
	});
	//alert(LEVEL)
}

function sfx(which, stop) {
	if(!GAME.savefile.soundfx) return;
	var audio = document.querySelector("audio." + which);
	audio.pause();
	audio.currentTime = 0;
	if(!stop) {
		audio.play();
	}
}

var AUTORUNNING = false
function tapcolumn(e, cheat, auto) {
	unwin(true);
	var target = e.target;
	var autofrom = null;
	var autoto = null;
	var ball = null;
	var ball2 = null;
	var empty = null;
	if(target.parentNode.className === "column") {
		target = target.parentNode;
	}
	var hold = document.querySelector("#hold");
	if(hold.firstChild.className === "empty") {
		ball = target.querySelector(".ball");
		if(ball) {
			target.insertBefore(hold.firstChild, ball);
			hold.appendChild(ball);
			if(!cheat) {
				if(!auto) {
					sfx("pop");
				}
				prev.push(target);
				document.querySelector("#undo").disabled = false;
			}
		} else {
			return false;
		}
	} else {
		empty = target.querySelectorAll(".empty");
		if(empty.length) {
			empty = empty[empty.length - 1];
			if(cheat || !empty.nextSibling ||
			   empty.nextSibling.className === hold.firstChild.className) {
				target.insertBefore(hold.firstChild, empty);
				hold.appendChild(empty);
				if(!cheat) {
					if(!auto) {
						sfx("push");
					}
					prev.push(target);
					if(prev.length >= 2) {
						autofrom = prev[prev.length - 2];
						autoto = target;
					}
					document.querySelector("#undo").disabled = false;
				}
			} else {
				return false;
			}
		}
	}
	if(!SHUFFLING) {
		if(GAME.savefile.autopile && autofrom && autoto && autofrom != autoto) {
			ball = autofrom.querySelector(".ball");
			ball2 = autoto.querySelector(".ball");
			empty = autoto.querySelector(".empty");
			var cont = document.querySelector("#bincontainer");
			if(empty && ball && ball2 && ball.className === ball2.className) {
				// auto move the next one
				if(!AUTORUNNING) {
					sfx("shuffle");
				}
				AUTORUNNING = true;
				setTimeout(function() {
					tapcolumn({ target: autofrom }, cheat, true);
					setTimeout(function() {
						tapcolumn({ target: autoto }, cheat, true);
					}, 25);
				}, 25);
			} else {
				if(AUTORUNNING) {
					setTimeout(function() {
						sfx("shuffle", true);
					}, 100);
					AUTORUNNING = false;
				}
			}
		}
		if(GAME.savefile.maxundo >= 0 && prev.length > GAME.savefile.maxundo) {
			prev = prev.splice(prev.length - GAME.savefile.maxundo);
		}
		if(!TIMER) {
			if(FROMSEED) {
				STARTED = new Date().valueOf();
			} else {
				STARTED = new Date().valueOf() - GAME.savefile.timer;
			}
			TIMER = setInterval(function() {
				settimer(new Date().valueOf() - STARTED);
			}, 1000);
		}
		checkwin();
	}
	return true;
}

function burp() {
	var cols = document.querySelectorAll(".column");
	var head = 0;
	var tail = cols.length - 1;
	var hold = document.querySelector("#hold");
	if(hold.firstChild.className !== "empty") {
		while(cols[head].firstChild.className !== "empty") {
			head++;
		}
		tapcolumn({target: cols[head]}, true);
	}
	if(!cols[tail].querySelector(".ball")) {
		tail--;
	}
	while(head < tail && cols[tail].querySelector(".ball")) {
		tapcolumn({target: cols[tail]}, true);
		while(cols[head].firstChild.className !== "empty") {
			head++;
		}
		tapcolumn({target: cols[head]}, true);
		if(!cols[tail].querySelector(".ball")) {
			tail--;
		}
	}
	document.querySelector("#bincontainer").classList.remove("notouch");
	checkwin(function() {
		//console.log("won automatically, retrying");
		shuffle_anim();
	});
	SHUFFLING = false;
	sfx("shuffle", true);
	document.querySelector("#undo").disabled = true;
	document.querySelector("#quit").disabled = false;
	document.querySelector("#retry").disabled = false;
	document.querySelector("#bins select").disabled = false;
	document.querySelector("#depth select").disabled = false;

	save();
}

function shuffle_step() {
	var cols = document.querySelectorAll(".column");
	var idx = 0;
	var dest = 0;
	var ball = null;
	idx = Math.floor(rand() % cols.length);
	// drill down through the whole bin and scatter it
	while(ball = cols[idx].querySelector(".ball")) {
		tapcolumn({target: cols[idx]}, true);
		dest = idx;
		// drop somewhere else with space available
		while(true) {
			dest = Math.floor(rand() % cols.length);
			if(dest === idx) continue;
			ball = cols[dest].querySelector(".empty");
			if(ball) {
				tapcolumn({target: cols[dest]}, true);
				break;
			}
		}
	}
}
function shuffle_anim(howmanymore) {
	SHUFFLING = true;
	unwin();
	document.querySelector("#shuffle").classList.add("hidden");
	document.querySelector("#bincontainer").classList.add("notouch");
	if(typeof howmanymore !== "number") {
		howmanymore = Math.floor(DEPTH * BINS);
		//howmanymore = 100;
		total = howmanymore;
	} else if(!howmanymore) {
		burp();
		return;
	}
	shuffle_step();
	setTimeout(shuffle_anim, 10, howmanymore - 1, total);
}
function shuffle(seedword) {
	if(!resetnag()) {
		document.querySelector("#bincontainer").classList.remove("notouch");
		document.querySelector("#retry").disabled = false;
		return;
	}
	setupboard();
	resize();
	seedword = seedword || genstring();
	SEED = seedword;
	if(!FROMSEED) {
		GAME.savefile.seed = SEED;
	}
	lastrand = hash(seedword);
	var link = document.querySelector("#seed");
	link.href = "#" + seedword;
	link.textContent = "seed: " + seedword;
	link.classList.remove("hidden");
	showcontinue(false);
	GAME.savefile.timer = 0;
	settimer(0);
	sfx("shuffle");
	shuffle_anim();

	document.querySelector("#quit").disabled = true;
	document.querySelector("#retry").disabled = true;
	document.querySelector("#bins select").disabled = true;
	document.querySelector("#depth select").disabled = true;
	document.querySelector("#shuffle").classList.add("hidden");
	document.querySelector("#continue").classList.add("hidden");
}

function checkwin(cb) {
	var fail = false;
	document.querySelectorAll(".column").forEach(function(col) {
		var contents = "";
		col.childNodes.forEach(function(entry) {
			if(!contents) {
				contents = entry.className;
				return;
			}
			if(entry.className === contents) {
				return;
			}
			fail = true;
		});
	});
	if(!fail) {
		if(cb) {
			cb();
		} else {
			win();
		}
	}
}
function win() {
	//GAME.savefile.timer = GAME.savefile.timer || new Date().valueOf();
	GAME.savefile.timer = new Date().valueOf() - STARTED;
	GAME.savefile.totaltime += GAME.savefile.timer;
	GAME.savefile.totalgames++;
	if(FROMSEED) {
		GAME.savefile.seedgames++;
	}
	clearInterval(TIMER);
	TIMER = null;

	//console.log(GAME.savefile.timer, STARTED);
	GAME.savefile.streakwon = GAME.savefile.streakwon || {};
	GAME.savefile.streakmax = GAME.savefile.streakmax || 0;
	if(!GAME.savefile.streakwon[SEED]) {
		GAME.savefile.streakwon[SEED] = GAME.savefile.timer;
		GAME.savefile.streak++;
		document.querySelector("#streak .n").textContent = GAME.savefile.streak;
		GAME.savefile.streakmax = Math.max(GAME.savefile.streak,
										   GAME.savefile.streakmax);
		localStorage.setItem("savefile", JSON.stringify(GAME.savefile));
	}
	var blurb = [
		"Success!", "Hooray!", "Victory!", "Bla-DOW!", "Kablammo!", "Boom!",
		"Yesss!", "w00t w00t!", "A WINNER IS YOU", "Wub-a-lub-a-dub DUB!"
	];
	var div = document.querySelector("#win");
	div.textContent = blurb[rand() % blurb.length];
	div.style.transform = "scale(1) rotate(" + (Math.random()*20 - 10) + "deg)";
	if(!FROMSEED) {
		document.querySelector("#shuffle").classList.remove("hidden");
		document.querySelector("#bincontainer").classList.add("notouch");
	}
	document.querySelector("#bincontainer").classList.add("notouch");
}
function unwin(skipbarrier) {
	var div = document.querySelector("#win");
	div.style.transform = "scale(0) rotate(0)";
	if(!skipbarrier) {
		document.querySelector("#bincontainer").classList.remove("notouch");
	}
}

function changebins(value, byIndex) {
	if(byIndex) {
		document.querySelector("#bins select").selectedIndex = value;
		BINS = document.querySelector("#bins select").value;
	} else {
		BINS = Number.parseInt(value, null);
		document.querySelector("#bins select").value = BINS;
	}
	if(!FROMSEED) {
		GAME.savefile.bins = BINS;
	}
	setupboard();
	document.querySelector("#shuffle").classList.remove("hidden");
	document.querySelector("#retry").disabled = true;
}
function changedepth(value, byIndex) {
	if(byIndex) {
		document.querySelector("#depth select").selectedIndex = value;
		DEPTH = document.querySelector("#depth select").value;
	} else {
		DEPTH = Number.parseInt(value, null);
		document.querySelector("#depth select").value = DEPTH;
	}
	if(!FROMSEED) {
		GAME.savefile.depth = DEPTH;
	}
	setupboard();
	document.querySelector("#shuffle").classList.remove("hidden");
}

function undo() {
	if(document.querySelector("#undo").disabled) return;
		if(!TIMER) {
			TIMER = setInterval(function() {
				settimer(new Date().valueOf() - STARTED);
			}, 1000);
		}
	unwin();
	tapcolumn({ target:prev.pop() }, true);
	if(!prev.length) {
		document.querySelector("#undo").disabled = true;
	}
	document.querySelector("#shuffle").classList.add("hidden");
	//prev = null;
}

function size() {
	var perheight = 2.2;
	var cont = document.querySelector("#bincontainer");
	if(cont.classList.contains("squish")) {
		perheight -= 1;
	}
	if(cont.classList.contains("harder")) {
		perheight -= 0.5;
	}
	document.querySelectorAll(".column").forEach(function(col, idx) {
		col.style.height = (perheight * DEPTH) + "em";
	});
}
function resize() {
	var cont = document.querySelector("#bincontainer");
	cont.classList.remove("squish");
	cont.classList.remove("harder");
	size();
	if(document.body.clientHeight < document.body.scrollHeight) {
		cont.classList.add("squish");
		size();
	}
	if(document.body.clientHeight < document.body.scrollHeight) {
		cont.classList.add("harder");
		size();
	}
}

// needs SEED set, sets up BINS and DEPTH
function setupselects() {
	var c = ["a", "b", "c", "d", "e", "f", "g", "h", "j", "k", "m", "n"];
	var bins = SEED.substring(SEED.length - 2, SEED.length - 1);
	var binsMax = document.querySelectorAll("#bins option").length;
	var binsIdx = c.indexOf(bins);
	if(binsIdx < 0 || binsIdx >= binsMax) {
		changebins(hash(bins) % binsMax, true);
	} else {
		changebins(binsIdx, true);
	}
	var depth = SEED.substring(SEED.length - 1);
	var depthIdx = c.indexOf(depth);
	var depthMax = document.querySelectorAll("#depth option").length;
	if(depthIdx < 0 || depthIdx >= depthMax) {
		changedepth(hash(depth) % depthMax, true);
	} else {
		changedepth(depthIdx, true);
	}
}

function playseed(seedword) {
	FROMSEED = true;
	document.querySelector("#mainmenu").classList.add("away");
	document.querySelector("#stats").classList.add("away");
	document.querySelector("#game").classList.remove("away");
	init(true);

	SEED = seedword;
	lastrand = hash(seedword);
	var link = document.querySelector("#seed");
	link.href = "#" + seedword;
	var txt = seedword;
	if(txt.length > 13) {
		txt = txt.substring(0, 10) + "...";
	}
	link.textContent = "seed: " + txt;
	link.classList.remove("hidden");

	setupselects();
	init(true);
	shuffle(seedword);
}

function setupboard() {
	var colors = [
		"red", "orange", "yellow", "green", "cyan", "blue", "purple", "pink"
	];
	var i = 0;
	var ball = null;
	var cont = document.querySelector("#bincontainer");
	var col = null;
	while(cont.firstChild) {
		cont.removeChild(cont.firstChild);
	}
	for(i = 0; i < BINS; i++) {
		col = document.createElement("div");
		col.className = "column";
		cont.appendChild(col);
	}
	cont.classList.add("notouch");
	document.querySelectorAll(".column").forEach(function(col, idx) {
		col.addEventListener("click", function(e) {
			if(!AUTORUNNING) {
				tapcolumn(e);
			}
		});
		if(idx < (BINS - 2)) {
			ball = null;
			for(i = 0; i < DEPTH; i++) {
				ball = document.createElement("div");
				ball.className = "ball " + colors[idx];
				col.appendChild(ball);
			}
		} else {
			for(i = 0; i < DEPTH; i++) {
				ball = document.createElement("div");
				ball.className = "empty";
				col.appendChild(ball);
			}
		}
	});
	save();
}

function showcontinue(show) {
	document.querySelector("#bincontainer").classList.toggle("notouch", show);
	document.querySelector("#continue").classList.toggle("hidden", !show);
	document.querySelector("#shuffle").classList.toggle("hidden", !show);
	document.querySelector("#retry").disabled = show;
	document.querySelector("#bins select").disabled = show;
	document.querySelector("#depth select").disabled = show;
}

function rendertime(msec) {
	var sec = Math.floor((msec / 1000) % 60);
	var min = Math.floor((msec / 1000 / 60) % 60);
	var hrs = Math.floor((msec / 1000 / 60 / 60));
	var str = "";
	if(min < 10) str += "0";
	str +=  min + ":";
	if(sec < 10) str += "0";
	str += sec;
	if(hrs) str = hrs + ":" + str;
	return str;
}
function settimer(msec) {
	document.querySelector("#timer").textContent = rendertime(msec);
}

function init(ignoresave) {
	FROMSEED = ignoresave || false;
	var cont = document.querySelector("#bincontainer");

	document.querySelector("#undo").disabled = true;
	prev = [];
	unwin();
	document.querySelector("#hold div").className = "empty";

	document.querySelector("#bins select").value = BINS.toString();
	document.querySelector("#depth select").value = DEPTH.toString();
	setupboard();

	document.querySelector("#streak .n").textContent = GAME.savefile.streak;
	document.querySelector("#help").classList.toggle("hidden",
													 !GAME.savefile.showhelp);
	document.querySelector("#streak").classList.toggle("hidden",
													 !GAME.savefile.showstreak);
	document.querySelector("#timer").classList.toggle("hidden",
													  !GAME.savefile.showtimer);
	cont.classList.toggle("open", GAME.savefile.openbins);
	cont.classList.toggle("colorblind", GAME.savefile.colorblind);
	document.querySelector("#hold").classList.toggle("colorblind", GAME.savefile.colorblind);

	showcontinue(false);
	if(ignoresave) {
		settimer(0);
		document.querySelector("#bins").classList.add("hidden");
		document.querySelector("#depth").classList.add("hidden");
	} else {
		settimer(GAME.savefile.timer);
		document.querySelector("#bins").classList.remove("hidden");
		document.querySelector("#depth").classList.remove("hidden");
	}

	if(!ignoresave) {
		if(GAME.savefile.bins) {
			changebins(GAME.savefile.bins);
		}
		if(GAME.savefile.depth) {
			changedepth(GAME.savefile.depth);
		}
		if(GAME.savefile.state) {
			if(GAME.savefile.state.length !== GAME.savefile.bins) {
        console.error("wrong number of bins, aborting load", GAME.savefile);
				resize();
				return;
				//console.error("wrong number of bins, fixing", GAME.savefile);
        //changebins(GAME.savefile.state.length);
			}
			if(GAME.savefile.state[0].length !== GAME.savefile.depth) {
				console.error("wrong depth, aborting load", GAME.savefile);
				resize();
				return;
				//console.error("wrong depth, fixing", GAME.savefile);
        //changedepth(GAME.savefile.state[0].length);
			}
			LEVEL = GAME.savefile.state;
			//console.log("i think things are ok", GAME.savefile);
			load();
			SEED = GAME.savefile.seed || "";
			var link = document.querySelector("#seed");
			link.href = "#" + SEED;
			link.textContent = "seed: " + SEED;
			link.classList.remove("hidden");
			showcontinue(true);
			checkwin(function() {
				document.querySelector("#continue").classList.add("hidden");
				document.querySelector("#bins select").disabled = false;
				document.querySelector("#depth select").disabled = false;
			});
		}
		if(GAME.savefile.hold) {
			document.querySelector("#hold div").className = GAME.savefile.hold;
		}
		if(GAME.savefile.level) {
			LEVEL = GAME.savefile.level;
		}
	}
	//save();
//	setupboard();

	resize();
}

function resetnag() {
	var won = false;
	checkwin(function() { won = true; });
	GAME.savefile.streakwon = GAME.savefile.streakwon || {};
	if(!won && GAME.savefile.streak && !GAME.savefile.streakwon[SEED]) {
		var conf = true;
		if(GAME.savefile.showstreak) {
			conf = confirm("are you sure?  your win streak will reset.");
		}
		if(conf) {
			GAME.savefile.streak = 0;
			GAME.savefile.streakwon = {};
			document.querySelector("#streak .n").textContent = GAME.savefile.streak;
		}
		return conf;
	}
	return true;
}

function quit(notreally) {
	if(TIMER) {
		GAME.savefile.totaltime += GAME.savefile.timer;
	}
	if(!FROMSEED && !SHUFFLING) {
		GAME.savefile.level = JSON.parse(JSON.stringify(LEVEL));
		save();
		GAME.savefile.hold = document.querySelector("#hold div").className;
		GAME.savefile.state = JSON.parse(JSON.stringify(LEVEL));
		LEVEL = JSON.parse(JSON.stringify(GAME.savefile.level));
		GAME.savefile.bins = BINS;
		GAME.savefile.depth = DEPTH;
		if(TIMER) {
			GAME.savefile.timer = new Date().valueOf() - STARTED;
		}
	}
	localStorage.setItem("savefile", JSON.stringify(GAME.savefile));
	clearInterval(TIMER);
	TIMER = null;
	if(!notreally) {
		document.querySelector("#mainmenu").classList.remove("away");
		document.querySelector("#game").classList.add("away");
	}
}

window.addEventListener("load", function() {
	// main menu
	document.querySelector("a.play").addEventListener("click", function(e) {
		document.querySelector("#mainmenu").classList.add("away");
		document.querySelector("#game").classList.remove("away");
		document.querySelector("#shuffle").classList.remove("hidden");
		//document.querySelector("#buttons").classList.add("invisible");

		//document.querySelector("#bins").classList.remove("invisible");
		//document.querySelector("#depth").classList.remove("invisible");
		
		//TODO: ask continue or new
		init();
	});

	// game screen
	var opt = null;
	var i = 0;
	var sel = document.querySelector("#bins select");
	for(i = 4; i <= 10; i++) {
		opt = document.createElement("option");
		opt.value = i;
		opt.textContent = i;
		opt.selected = (i === BINS);
		sel.appendChild(opt);
	}
	sel.addEventListener("change", function(e) {
		if(!resetnag()) {
			sel.value = BINS;
			return false;
		}
		GAME.savefile.hold = null;
		GAME.savefile.state = null;
		changebins(this.value);
		init();
	});
	var dsel = document.querySelector("#depth select");
	for(i = 3; i <= 30; i++) {
		if(i > 10 && i % 5) continue;
		opt = document.createElement("option");
		opt.value = i;
		opt.textContent = i;
		opt.selected = (i === DEPTH);
		dsel.appendChild(opt);
	}
	dsel.addEventListener("change", function(e) {
		if(!resetnag()) {
			dsel.value = DEPTH;
			return false;
		}
		GAME.savefile.hold = null;
		GAME.savefile.state = null;
		changedepth(this.value);
		init();
	});

	document.querySelector("#win").addEventListener("click", unwin);

	// game buttons
	document.querySelector("#continue").addEventListener("click", function() {
		showcontinue(false);
	});
	document.querySelector("#shuffle").addEventListener("click", function() {
		shuffle();
	});
	document.querySelector("#quit").addEventListener("click", function(e) {
		quit();
	});

	// save on abandon
	document.addEventListener("visibilitychange", function() {
		if(document.visibilityState === "hidden") {
			quit(true);
		}
	});

	// init a seeded game
	if(window.location.hash && window.location.hash.length > 2) {
		playseed(window.location.hash.substring(1));
	}
});
window.addEventListener("keydown", function(e) {
	//unwin();
	//console.log(e);
	var num = Number.parseInt(e.key || "", null);
	if(num === 0 )num=10;
	var cols = document.querySelectorAll(".column");
	if(!Number.isNaN(num) && num <= cols.length) {
		if(!AUTORUNNING) {
			tapcolumn({target: cols[num - 1]});
		}
	} else if(e.key === "0" && cols.length >= 10) {
		if(!AUTORUNNING) {
			tapcolumn({target: cols[9]});
		}
	} else {
		if(e.key === "r" || e.key === "Escape") {
			if(!document.querySelector("#retry").disabled && !e.ctrlKey) {
				e.preventDefault();
				e.stopPropagation();
				load();
			}
		} else if(e.key === "q") {
			quit();
		} else if(e.key === "c") {
			if(!document.querySelector("#continue").classList.has("hidden")) {
				showcontinue(false);
			}
		} else if(e.key === "u") {
			if(!document.querySelector("#undo").disabled) {
				undo();
			}
		} else if(e.key === "s" || e.key === " ") {
			if(!document.querySelector("#shuffle").classList.has("hidden")) {
				shuffle();
			}
		}
	}
});
window.addEventListener("resize", resize);
