function seedinfo(seed) {
	var info = {
		bins: 0,
		depth: 0
	};
	var c = ["a", "b", "c", "d", "e", "f", "g", "h", "j", "k", "m", "n"];
	var opts = document.querySelectorAll("#bins option");
	var bins = seed.substring(seed.length - 2, seed.length - 1);
	var binsMax = opts.length;
	var binsIdx = c.indexOf(bins);
	if(binsIdx < 0 || binsIdx >= binsMax) {
		binsIdx = hash(bins) % binsMax;
	}
	info.bins = opts[binsIdx].value;

	opts = document.querySelectorAll("#depth option");
	var depth = seed.substring(seed.length - 1);
	var depthIdx = c.indexOf(depth);
	var depthMax = document.querySelectorAll("#depth option").length;
	if(depthIdx < 0 || depthIdx >= depthMax) {
		depthIdx = hash(bins) % binsMax;
	}
	info.depth = opts[depthIdx].value;
	return info;
}

function loadrecords() {
	[
		"streak", "streakmax", "totaltime", "totalgames", "seedgames"
	].forEach(function(item) {
		var txt = GAME.savefile[item];
		if(item === "totaltime") {
			txt = rendertime(txt);
		}
		document.querySelector("#stats ." + item).textContent = txt;
	});

	document.querySelector("#log").classList.toggle("hidden",
													!GAME.savefile.streak);
	if(!GAME.savefile.streak) {
		document.querySelector("#stats .favbins").textContent = "?";
		document.querySelector("#stats .favdepth").textContent = "?";
		return;
	}
	var tbody = document.querySelector("#stats tbody.streakwon");
	while(tbody.firstChild) tbody.removeChild(tbody.firstChild);
	GAME.savefile.streakwon = GAME.savefile.streakwon || {};
	var tr = null;
	var td = null;
	var info = null;
	var txt = "";
	var a = null;
	var favBins = {};
	var favDepth = {};
	for(var item in GAME.savefile.streakwon) {
		tr = document.createElement("tr");
		td = document.createElement("td");
		//time, seed, bins, depth
		td.textContent = rendertime(GAME.savefile.streakwon[item]);
		tr.appendChild(td);

		td = document.createElement("td");
		td.className = "seed";
		td.title = item;
		txt = item;
		if(txt.length > 13) {
			txt = txt.substring(0, 10) + "...";
		}
		a = document.createElement("a");
		a.href = "#" + item;
		a.title = item;
		a.textContent = txt;
		a.addEventListener("click", function(e) {
			playseed(e.target.href.split("#")[1]);
		});
		td.appendChild(a);
		tr.appendChild(td);

		info = seedinfo(item);
		favBins[info.bins] = favBins[info.bins] || 0;
		favBins[info.bins]++;
		favDepth[info.depth] = favDepth[info.depth] || 0;
		favDepth[info.depth]++;
		td = document.createElement("td");
		td.textContent = info.bins;
		tr.appendChild(td);
		td = document.createElement("td");
		td.textContent = info.depth;
		tr.appendChild(td);

		tbody.appendChild(tr);
	}

	var max = 0;
	var key = "";
	for(var idx in favBins) {
		if(favBins[idx] >= max) {
			max = favBins[idx];
			key = idx;
		}
	}
	document.querySelector("#stats .favbins").textContent = key;
	max = 0;
	for(var idx in favDepth) {
		if(favDepth[idx] >= max) {
			max = favDepth[idx];
			key = idx;
		}
	}
	document.querySelector("#stats .favdepth").textContent = key;
}

window.addEventListener("load", function() {
	var savefile = localStorage.getItem("savefile");
	if(savefile) {
		try {
			GAME.savefile = JSON.parse(savefile);
		} catch(ignore) {}
	}
	var sample = document.querySelector("#prefs .floater");
	var checks = [
		"showhelp", "showstreak", "showtimer",
		"colorblind", "openbins", "soundfx", "autopile"
	];
	checks.forEach(function(item) {
		var chk = document.querySelector("#" + item);
		chk.checked = GAME.savefile[item];
		chk.addEventListener("change", function(e) {
			GAME.savefile[item] = e.target.checked;
			sample.classList.toggle(item, e.target.checked);
		});
	});
	document.querySelector("#maxundo").addEventListener("change", function(e) {
		GAME.savefile.maxundo = parseInt(e.target.value, null);
	});

	var setoptions = function setoptions() {
		checks.forEach(function(item) {
			var chk = document.querySelector("#" + item);
			chk.checked = GAME.savefile[item];
		});
		["colorblind", "openbins"].forEach(function(item) {
			document.querySelector("#mainmenu h2").classList.toggle(
				item, document.querySelector("#" + item).checked);
		});
		if(typeof GAME.savefile.maxundo === "undefined") {
			GAME.savefile.maxundo = 1;
		}
		document.querySelector("#maxundo").value = GAME.savefile.maxundo;
	}
	setoptions();

	document.querySelector("a.seed").addEventListener("click", function(e) {
		var word = "";
		while(word !== null && word.length < 2) {
			word = prompt("enter any seed name");
		}
		if(word) {
			playseed(word);
		}
	});

	document.querySelector("a.records").addEventListener("click", function(e) {
		document.querySelector("#mainmenu").classList.add("away");
		document.querySelector("#stats").classList.remove("away");
		loadrecords();
		setoptions();
	});
	document.querySelector("#backup").addEventListener("click", function(e) {
		var a = document.querySelector("#download");
		a.href = "data:text/plain;base64," + btoa(JSON.stringify(GAME.savefile,
																 null, 4));
		a.click();
	});
	var upload = document.querySelector("#upload");
	upload.addEventListener("change", function(e) {
		var reader = new FileReader();
		reader.addEventListener("load", function(e) {
			var data = null;
			try {
				data = JSON.parse(e.target.result);
				if(confirm("confirm: replace current save data?")) {
					GAME.savefile = data;
					localStorage.setItem("savefile",
										 JSON.stringify(GAME.savefile));
					loadrecords();
					setoptions();
				}
			} catch(err) {
				alert("error parsing file; data was not accepted.");
				return;
			}
		});
		reader.readAsText(this.files[0]);
	});
	document.querySelector("#restore").addEventListener("click", function(e) {
		upload.click();
	});
	document.querySelector("#erase").addEventListener("click", function(e) {
		if(confirm("confirm: erase all records?")) {
			["hold", "state", "level", "timer"].forEach(function(item) {
				GAME.savefile[item] = null;
			});
			//GAME.savefile.bins = 8;
			//GAME.savefile.depth = 4;
			//GAME.savefile.maxundo = 0;
			[
				"streak", "streakmax", "totaltime", "totalgames", "seedgames"
			].forEach(function(item) {
				GAME.savefile[item] = 0;
			});
			GAME.savefile.streakwon = {};
			localStorage.setItem("savefile", JSON.stringify(GAME.savefile));
			loadrecords();
		}
	});
	document.querySelector("#stats .close").addEventListener("click", function(e) {
		document.querySelector("#mainmenu").classList.remove("away");
		document.querySelector("#stats").classList.add("away");
	});

	document.querySelector("a.prefs").addEventListener("click", function(e) {
		checks.forEach(function(item) {
			var chk = document.querySelector("#" + item);
			chk.checked = GAME.savefile[item];
			sample.classList.toggle(item, chk.checked);
		});
		document.querySelector("#mainmenu").classList.add("away");
		document.querySelector("#prefs").classList.remove("away");
	});
	document.querySelector("#save").addEventListener("click", function(e) {
		localStorage.setItem("savefile", JSON.stringify(GAME.savefile));
		setoptions();
		document.querySelector("#mainmenu").classList.remove("away");
		document.querySelector("#prefs").classList.add("away");
	});

	document.querySelector("a.credits").addEventListener("click", function(e) {
		document.querySelector("#mainmenu").classList.add("away");
		document.querySelector("#credits").classList.remove("away");
	});
	document.querySelector("#credits .close").addEventListener("click", function(e) {
		document.querySelector("#mainmenu").classList.remove("away");
		document.querySelector("#credits").classList.add("away");
	});

});
